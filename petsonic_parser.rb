#!/usr/bin/env ruby
# encoding: utf-8

require 'curb'
require 'nokogiri'
require 'csv'

class PetsonicParser

  def args_err
    $stderr.puts <<-INFO
  Args error
  Usage:
    #{__FILE__} <url> <output_file>
    INFO
    exit 1
  end

  def initialize 
    args_err if ARGV.length != 2
    @categ_url, @output_path = ARGV

    @categ_url += "?n=#{get_products_count}"
    
    easy_options = {:follow_location => true}
    multi_options = {:pipeline => Curl::CURLPIPE_HTTP1}

    CSV.open(@output_path, 'wb') do |csv|
      Curl::Multi.get(get_products_urls, easy_options, multi_options) do |easy|
        get_products(Nokogiri::HTML(easy.body_str)).each { |p| csv << p }
      end
    end
  end
  
  def get_page
    categ_page = Curl::Easy.perform @categ_url
    Nokogiri::HTML(categ_page.body_str)
  end
  
  def get_products_count
    get_page.xpath('.//input[@id="nb_item"]').attr 'value'
  end

  def get_products_urls
    links = get_page.xpath('.//div[@class="productlist"]//a[@class="product_img_link"]')
    links.map { |l| l.attr 'href' }
  end

  def single_product_page(page, name)
    attr_price = page.xpath('.//span[@id="price_display"]').first.content.strip!
    img = page.xpath('.//img[@id="bigpic"]').first.attr 'src'
    [["#{name}", attr_price, img]]
  end

  def get_products(page)
    prodname = page.xpath('.//div[@class="product-name"]//h1').first
    prodname = prodname.content.strip!.gsub(/\s+/, " ")
    pictures = page.xpath('.//ul[@id="thumbs_list_frame"]//li//a').map {|p| p.attr 'href'}
    products = page.xpath('.//ul[@class="attribute_labels_lists"]')
    return single_product_page(page, prodname) if products.length.zero?

    pictures *= products.length if pictures.length < products.length
    products.zip(pictures).map do |prod, img_url|
      attr_name = prod.xpath('.//span[@class="attribute_name"]').first.content
      attr_price = prod.xpath('.//span[@class="attribute_price"]').first.content.strip!
      ["#{prodname} #{attr_name}", attr_price, img_url]
    end
  end
end

PetsonicParser.new
